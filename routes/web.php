<?php

Route::get('index', function () {
    return view('UserViews.index2');
})->name('home');
Route::post('login','LoginController@login')->name('login');
Route::get('logout','LoginController@logout')->middleware('auth')->name('logout');
Route::get('change-password/first-login/{token}','ChangePasswordController@checkUrl')->name('check.url');
Route::post('change-password','ChangePasswordController@changePassFirstTime')->name('change.password.first');
Route::get('forgot-password','UserController@viewForgotPasswordPage')->name('forgot.pass.view');
Route::get('view-list-staff-in-department/{id}','UserController@viewListStaff')->middleware('auth','checkRouteDepartmentId')->name('view.list.staff.in.department');
Route::post('process-list-staff-in-department/{id}','UserController@getJsonListStaff')->middleware('auth','checkRouteDepartmentId')->name('get.json.list.staff.in.department');

Route::prefix('director')->group(function (){
    Route::post('delete-user','DirectorController@deleteUser')->name('delete.user');
    Route::get('index','DirectorController@index')->middleware('auth','checkrole')->name('director.view');
    Route::post('index','DirectorController@searchUser')->middleware('auth')->name('search.user');
    Route::post('register','RegisterController@register')->name('create.new.account');
    Route::get('register','DirectorController@viewRegister')->middleware('auth','checkrole')->name('register');
    Route::get('view-profile/{id}','DirectorController@viewProfile')->middleware('auth','checkRouteId')->name('view.profile.by.manager');
    Route::post('profile/change-information-by-manager','UserController@changeInformationByManager')->middleware('auth','checkAssignmentDepartment')->name('change.information.by.manager');
    Route::post('profile/assignment-manager-department','DirectorController@assignmentManagementDepartment')->middleware('auth','checkAssignmentDepartment')->name('assignment.manager.department');
    Route::post('reset-password','DirectorController@resetPassword')->middleware('auth')->name('reset.password');
    Route::get('view-list-new-employees','DirectorController@viewListNewEmployee')->middleware('auth','checkrole')->name('list.new.employees');
    Route::post('view-list-new-employees-json','DirectorController@viewListNewEmployees')->middleware('auth')->name('get.list.new.employees');
    Route::get('manager/department','DirectorController@getListDepartment')->middleware('auth','checkrole')->name('view.list.department');
    Route::get('manager/department-no-manager','DirectorController@getListDepartmentNoManager')->middleware('auth','checkrole')->name('view.list.department.no.manager');
    Route::post('create/department','DirectorController@createDepartment')->middleware('auth','checkNameCreateDepartment')->name('create.department');
    Route::post('delete/department','DirectorController@deleteDepartment')->middleware('auth')->name('delete.department');
    Route::post('edit/department','DirectorController@editDepartment')->middleware('auth','checkNameDepartment')->name('edit.department');
    Route::get('view/detail-department/{id}','DirectorController@viewDetailDepartment')->middleware('auth','checkDepartmentID')->name('view.detail.department');
    Route::post('delete/user-from-department','DirectorController@deleteUserFromDepartment')->middleware('auth')->name('delete.user.from.department');
    Route::post('delete/manager-rights','DirectorController@deleteManagerRights')->middleware('auth')->name('delete.manager.rights');
    Route::get('manager/create-new-department','DirectorController@createNewDepartment')->middleware('auth','checkrole')->name('create.new.department');
    Route::post('manager/department','DirectorController@searchDepartmentName')->middleware('auth')->name('search.department.name');
});

Route::prefix('user')->group(function (){
    Route::post('forgot-password','UserController@forgotPassword')->name('forgot.pass.by.mail');
    Route::get('profile','UserController@viewProfile')->middleware('auth')->name('profile');
    Route::post('profile/change-information','UserController@changeInformation')->middleware('auth')->name('change.information');
    Route::post('profile/change-password','UserController@changePassword')->middleware('auth')->name('change.password');
    Route::post('profile/change-avatar','UserController@changeAvatar')->middleware('auth')->name('change.avatar');
    Route::get('list-department-staff/datatable','UserController@viewListStaffDepartmentDatatable')->middleware('auth')->name('view.list.staff.department.datatable');
    Route::post('list-department-staff/datatable-post','UserController@getListStaff')->middleware('auth')->name('get.list.staff.in.department');
    Route::get('list-managing-department-page','UserController@viewListManagingDepartment')->middleware('auth','checkManagingDepartment')->name('view.managing.department.page');
    Route::post('leave/manager-department','UserController@leaveManagerDepartment')->middleware('auth')->name('leave.manager.department');
    Route::post('list-managing-department-page','UserController@searchListDepartmentManaging')->middleware('auth')->name('search.list.department.managing');

    Route::get('manager/export-excel','ExportController@exportToExcel')->name('export.excel');
});