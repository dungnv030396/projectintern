<?php

namespace App\Http\Middleware;

use App\DepartmentName;
use Closure;

class checkNameCreateDepartment
{
    /**
     * Check department's name is exist or not
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $departments = DepartmentName::all();

        foreach ($departments as $department){
            if($department->name === request('name')){
                alert()->error('The department name was exist');
                return redirect()->back()->with('errorName','message');
            }
        }
        return $next($request);
    }
}
