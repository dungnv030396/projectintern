<?php

namespace App\Http\Middleware;

use App\ManagerDepartment;
use App\User;
use Closure;

class CheckAssignmentManagerment
{
    /**
     * Check the department has a manager or not
     * Check user selected in drop down or not
     * If user has no department then can't receive manage department
     * If user has not department Manager so can't receive manage department Manager
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = User::find(\request('id'));
        $department_id = request('department');
        $managerDepartments = ManagerDepartment::all();
        if ($department_id) {
            foreach ($managerDepartments as $item) {
                foreach ($department_id as $id) {
                    if ($item->department_id == $id) {
                        alert()->error('Sorry, This department has a manager')->persistent('Close');
                        return redirect()->back()->with('noDepartment', 'message');
                    }
                }
            }
        }
//        if (is_null($department_id)) {
//            alert()->error('Sorry, You must select at least one department')->persistent('Close');
//            return redirect()->back()->with('selectNull', 'message');
////        } elseif ($user->department_id == null) {
//        }
        if (request('department_id') == '' && !is_null($department_id)) {
            alert()->error('Sorry, User has no department can not manage other departments')->persistent('Close');
            return redirect()->back()->with('noDepartment', 'message');
        } elseif(!is_null($department_id)) {
            foreach ($department_id as $item) {
                if (request('department_id') != 2 && $item == 2) {
                    alert()->error('Sorry, The current user can not manage the Manager department')->persistent('Close');
                    return redirect()->back()->with('noDepartment', 'message');
                }
            }
        }

        return $next($request);
    }
}