<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class checkRole
{
    /**
     * Check role_id to use some function
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->role_id!=3){
            alert()->error("You don't have rights to use this function");
            return redirect()->back()->with('noRights','message');
        }
        return $next($request);
    }
}
