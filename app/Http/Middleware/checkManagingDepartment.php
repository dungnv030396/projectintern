<?php

namespace App\Http\Middleware;

use App\ManagerDepartment;
use Closure;
use Illuminate\Support\Facades\Auth;

class checkManagingDepartment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $mDepartment = ManagerDepartment::where('user_id', Auth::user()->id)->count();
        if (Auth::user()->role_id != 3) {
            if ($mDepartment == 0) {
                alert()->error('Sorry,You do not have rights to view this page')->persistent('Close');
                return redirect()->back()->with('noRight', 'message');
            }
        }
        return $next($request);
    }
}
