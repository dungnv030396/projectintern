<?php

namespace App\Http\Middleware;

use App\DepartmentName;
use Closure;

class checkDepartmentId
{
    /**
     * Check department_id exist or not
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $department_id = request('id');
        $departments = DepartmentName::all();
        $count = 0;
        foreach ($departments as $department){
            if($department_id == $department->id ){
                $count++;
            }
        }
        if($count == 0){
            alert()->error('This Department ID not exist');
            return redirect()->back()->with('errorID','message');
        }else{
            return $next($request);
        }
    }
}
