<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use PHPUnit\Framework\Constraint\Count;

class checkRouteId
{
    /**
     * Check user_id has exist or not
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $id = request('id');
        $user = User::where('status',1)
            ->where('role_id','!=',3)
            ->where('id',$id)->get();
        if (\count($user)==0){
            alert()->error('404 ERROR - Link has expired ');
            return redirect()->back()->with('404notfound','mesasge');
        }
        return $next($request);
    }
}
