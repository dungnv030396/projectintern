<?php

namespace App\Http\Middleware;

use App\DepartmentName;
use App\ManagerDepartment;
use Closure;
use Illuminate\Support\Facades\Auth;

class checkRightsDepartment
{
    /**
     * Check if role_id = 3, so user can view all department
     * Check department_id on url is exist or not
     * and check user has rights to view the department on the url
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $departmentIdRoute = request('id');
        $error = 0;
//        dd($departmentIdRoute);
        if(Auth::user()->role_id == 3){
            $alldepartment = DepartmentName::all();
            foreach ($alldepartment as $item){
                if ($departmentIdRoute == $item->id){
                    $error++;
                }
            }
            if ($error ==0){
                alert()->error("Sorry, Do not have this department.")->persistent('Close');
                return redirect()->back()->with('error','message');
            }
        }else{
            $mDepartment = ManagerDepartment::where('user_id',Auth::user()->id)->get();
            foreach ($mDepartment as $item){
                if($departmentIdRoute == $item->department_id){
                    $error++;
                }
            }
            if ($error==0){
                alert()->error("You don't have rights to view this page")->persistent('Close');
                return redirect()->back()->with('error','message');
            }
        }

        return $next($request);
    }
}
