<?php

namespace App\Http\Controllers;

use App\DepartmentName;
use App\Http\Requests\CreateDepartmentRequest;
use App\Mail\ResetPasswordMail;
use App\ManagerDepartment;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class DirectorController extends Controller
{
    /**
     * get list User without director
     * receive object user and redirect to view with that object user
     *
     * @return resources\views\DirectorViews\list_user.blade.php with object $listEmployee
     */
    public function index()
    {
        $user = new User();
        $listEmployee = $user->getEmployeeByDirector();
        return view('DirectorViews.list-user', compact('listEmployee'));
    }

    /**
     * director search user by user's information
     * receive object user from searchUser() function at Model User
     *
     * @return resources\views\DirectorViews\list_user.blade.php with object $listEmployee
     */
    public function searchUser()
    {
        $user = new User;
        $listEmployee = $user->searchUser();
        return view('DirectorViews.list-user', compact('listEmployee'));
    }

    /**
     * receive object $listDepartment from getListDepartment() function at DepartmentName Model
     *
     * @return resources\views\DirectorViews\create_new_department.blade.php with object $listDepartment
     */
    public function getListDepartment()
    {
        $dName = new DepartmentName();
        $listDepartment = $dName->getListDepartment();
        return view('DirectorViews.manager-department', compact('listDepartment'));
    }

    /**
     * receive object $listDepartment from getListDepartmentNoManager() function at DepartmentName Model
     *
     * @return resources\views\DirectorViews\create_new_department.blade.php with object $listDepartment
     */
    public function getListDepartmentNoManager()
    {
        $dName = new DepartmentName();
        $listDepartment = $dName->getListDepartmentNoManager();
        return view('DirectorViews.manager-department', compact('listDepartment'));
    }

    /**
     * Director redirect to list new employees page
     *
     * @return resources\views\DirectorViews\list-new-employees.blade.php
     */
    public function viewListNewEmployee()
    {
        return view('DirectorViews.list-new-employees');
    }

    /**
     * Director get request and save new department
     *
     * @param CreateDepartmentRequest $request
     * @return back resources\views\DirectorViews\manager-department.blade.php with message 'success'
     */
    public function createDepartment(CreateDepartmentRequest $request)
    {
        $department = new DepartmentName();
        $department->name = \request('name');
        $department->save();

        if (!is_null(request('manager_id'))){
            ManagerDepartment::create([
                'department_id' => $department->id,
                'user_id' => \request('manager_id'),
            ]);
        }
        alert()->success('Create New Department Successful');
        return redirect()->route('view.list.department')->with('createSuccess', 'message');
    }

    /**
     * Director delete department
     *
     * @return back resources\views\DirectorViews\manager-department.blade.php with message 'success'
     */
    public function deleteDepartment()
    {
        $department_id = \request('id');
        $users = User::where('department_id',$department_id)->get();
        foreach ($users as $user){
            $user->department_id = null;
            $user->save();
        }
        DepartmentName::find($department_id)->delete();
        ManagerDepartment::where('department_id',$department_id)->delete();
        return redirect()->back();
    }

    /**
     * Director delete(change status =0) user
     *
     * @return back \resources\views\DirectorViews\list-user.blade.php
     */
    public function deleteUser()
    {
        $user = User::find(\request('id'));
        $user->status = 0;
        $user->save();
        return redirect()->back();
    }

    /**
     * Director can view User's profile
     *
     * @return \resources\views\UserViews\view_profile_by_manager.blade.php with object user,departments,department_manager and array dName
     */
    public function viewProfile()
    {
        $user = User::find(\request('id'));
        $departments = DepartmentName::all();
        $department_manager = ManagerDepartment::where('user_id', Auth::user()->id)->get();
        $managerDepartments = $user->managerDepartments;
        $dName = array();
        foreach ($managerDepartments as $item) {
            $department = DepartmentName::find($item->department_id);

            array_push($dName, $department->name);
        }
        return view('UserViews.view_profile_by_manager', compact('user', 'departments', 'department_manager', 'dName'));
    }

    /**
     * Director redirect to add new account page
     *
     * @return \resources\views\DirectorViews\register.blade.php
     */
    public function viewRegister()
    {
        return view('DirectorViews.register');
    }

    /**
     * Director assignment to user manager department
     *
     * @return back \resources\views\UserViews\view_profile_by_manager.blade.php with message 'assignmentSuccess'
     */
    public function assignmentManagementDepartment()
    {
        $department_id = \request('department');
        foreach ($department_id as $item) {
            $managerDepartment = new ManagerDepartment();
            $managerDepartment->user_id = \request('id');
            $managerDepartment->department_id = $item;
            $managerDepartment->save();
        }
        alert()->success('Assignment Management Department Successful');
        return redirect()->back()->with('assignmentSuccess', 'message');
    }

    /**
     * Director can reset password 1 or multi user's account and send a mail to the user.
     *
     * @return \resources\views\DirectorViews\list_user.blade.php with message 'noSelect' or 'resetSuccess'
     */
    public function resetPassword()
    {
        $listID = \request('checkbox_id');
        if ($listID == null) {
            alert()->error('Error Message', 'Please select one row');
            return redirect()->back()->with('noSelect', 'message');
        } else {
            foreach ($listID as $id) {
                $user = User::find($id);
                $user->password = bcrypt('123a123');
                $user->save();
                Mail::to($user)->send(new ResetPasswordMail($user));
            }
            alert()->success('Success Message', 'Reset password Successful');
            return redirect()->back()->with('resetSuccess', 'message');
        }
    }

    /**
     * Get json for list new employees from viewListNewEmployees() function at Model User
     *
     * @param Request $request
     */
    public function viewListNewEmployees(Request $request)
    {
        $user = new User();
        $start = $request->input('start') ?: 0;
        $length = $request->input('length') ?: 10;
        $search = $request->input('search.value') ?: "";
        $orderColunm = $request->input('order.0.column') ?: 0;
        $oderSortType = $request->input('order.0.dir') ?: 'asc';
        $draw = $request->draw ?: 0;
        try {
            $output = $user->viewListNewEmployees($start, $length, $search, $orderColunm, $oderSortType, $draw);
        } catch (\Exception $ex) {
            $output = array(
                "draw" => intval(0),
                "recordsTotal" => intval(0),
                "recordsFiltered" => intval(0),
                "data" => array($start . "," . $length . "," . $search . "," . $orderColunm . "," . $oderSortType . "," . $draw),
            );
        }
        echo json_encode($output);
    }

    /**
     * Manager can delete user in the their managing department
     *
     */
    public function deleteUserFromDepartment()
    {
        $user = User::find(\request('id'));
        if ($user) {
            $user->department_id = null;
            $user->save();
            ManagerDepartment::where('user_id', $user->id)->delete();
        } else {
            dd('have not found this account');
        }
    }

    /**
     * Director can redirect to detail department page
     *
     * @return \resources\views\UserViews\edit-department.blade.php with object departments,managers,mDepartment
     */
    public function viewDetailDepartment()
    {
        $managers = User::where('status', 1)
            ->where('department_id', '<>', 1)
            ->get();
//        dd($managers);
        $departments = DepartmentName::find(request('id'));
        $mDepartment = ManagerDepartment::where('department_id', $departments->id)->first();
        return view('DirectorViews.edit-department', compact('departments', 'managers', 'mDepartment'));
    }

    /**
     * director can edit department's information
     *
     * @return back \resources\views\UserViews\edit-department.blade.php with message 'errorName'
     */
    public function editDepartment()
    {

        DepartmentName::find(request('department_id'))->update([
            'name' => trim(\request('name'))
        ]);
        if (!is_null(request('manager_id'))) {
            $mDepartment = ManagerDepartment::where('department_id', \request('department_id'))
                ->first();
            if (count($mDepartment) > 0) {
                $mDepartment->update([
                    'user_id' => \request('manager_id'),
                    'updated_at' => Carbon::now()->addHour(7)->toDateTimeString(),
                ]);
            } else {
                ManagerDepartment::create([
                    'department_id' => request('department_id'),
                    'user_id' => \request('manager_id'),
                ]);
            }
        }
        alert()->success('Edit department information successful')->persistent('Close');
        return redirect()->route('view.list.department')->with('errorName', 'message');
    }

    /**
     * Director can delete manager of department
     */
    public function deleteManagerRights()
    {
        $department_id = \request('id');
        ManagerDepartment::where('department_id', $department_id)->delete();
    }

    /**
     * Director can redirect to create new department view
     * and send object manager to view
     *
     * @return \resources\views\DirectorViews\create-new-department.blade.php
     */
    public function createNewDepartment(){
        $managers = User::where('status', 1)
            ->where('department_id', '<>', 1)
            ->get();
        return view('DirectorViews.create-new-department',compact('managers'));
    }

    /**
     * Director can search search department name and manager name
     * and send a object $listDepartment to view from searchDepartmentName()function
     *
     * @return \resources\views\DirectorViews\manager-department.blade.php
     */
    public function searchDepartmentName(){
        $dName = new DepartmentName();
        $listDepartment = $dName->searchDepartmentName();
        return view('DirectorViews.manager-department',compact('listDepartment'));
    }

}
