<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * receive data (role_id) from login() function at Model User
     *
     * @return \resources\views\UserViews\index2.blade.php if error
     *         \resources\views\DirectorViews\list-user.blade.php if role_id = 3
     *         \resources\views\UserViews\profile.blade.php  if other role_id
     *
     */
    public function login(){
        $user = new User();
        $res = $user->login();
        if ($res==0){
            return redirect()->back()->withErrors('Username or Password invalid');
        }else{
            switch ($res){
                case 3:
                    return redirect()->route('director.view');
                    break;
                default:
                    return redirect()->route('profile');
                    break;
            }
        }
    }

    /**
     * logout and clear User's session
     *
     * @return \resources\views\UserViews\index2.blade.php
     */
    public function logout(){
        $user = new User();
        $res = $user->doLogout();
        if(!$res){
            return redirect()->route('home');
        }else{
            dd("can't logout");
        }
    }
}
