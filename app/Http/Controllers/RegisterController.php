<?php

namespace App\Http\Controllers;

use App\DepartmentName;
use App\Http\Requests\RegistrationRequest;
use App\ManagerDepartment;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class RegisterController extends Controller
{
    /**
     * Director create new account
     * receive object user from createNewAccount() function at Model User
     *
     * @param RegistrationRequest $request
     * @return \resources\views\DirectorViews\register.blade.php with message 'success' or error message
     */
    public function register(RegistrationRequest $request){
        $user = new User();
        $res = $user->createNewAccount();
        if($res =='email'){
            alert()->success('Email already exists');
            return redirect()->route('register')->with('registerSuccess','message');
        }else if($res =='user'){
            alert()->success('UserName already exists');
            return redirect()->route('register')->with('registerSuccess','message');
        }if (is_null($res)){
            alert()->success('Something wrong');
            return redirect()->back()->with('registerSuccess','message');
        }
        alert()->success('Register Successful');
        return redirect()->back()->with('registerSuccess','message');
    }
}
