<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePassRequest;
use App\PasswordToken;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class ChangePasswordController extends Controller
{
    /**
     * User have to change password at first time login
     *
     * @param ChangePassRequest $request
     * @return \resources\views\UserViews\index2.blade.php with message 'success'
     */
    public function changePassFirstTime(ChangePassRequest $request){
        $user = User::find(\request('user_id'));
        $user->password = bcrypt(\request('password'));
        $user->save();
        $pToken = PasswordToken::where('user_id',\request('user_id'))->first();
        $pToken->status = 0;
        $pToken->save();
        return Redirect::route('home')->with('success','Change password successfull');
    }

    /**
     * check the url exists or not
     *
     * @return \resources\views\UserViews\change-pass-by-mail.blade.php
     */
    public function checkUrl(){
        $tokens = PasswordToken::all();
        foreach ($tokens as $token){
            if (Hash::check(\request('token').''.'rikkei',$token->token)) {
                if($token->status==1){
                    $user_id = $token->user_id;
                    return view('UserViews.change-pass-by-mail',compact('user_id'));
                }else{
                    return view('welcome');
                }
            }
        }
    }
}
