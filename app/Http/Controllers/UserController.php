<?php

namespace App\Http\Controllers;

use App\DepartmentName;
use App\Exports\UsersExport;
use App\Exports\UsersExportExcel;
use App\Http\Requests\ChangeInfoRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\ManagerDepartment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Excel;

class UserController extends Controller
{
    /**
     * receive object user from forgotPassword() function at Model User
     *
     * @return \resources\views\UserViews\forgot-pass.blade.php with message 'emailFound' or message error
     */
    public function forgotPassword()
    {
        $user = new User();
        $res = $user->forgotPassword();
        if ($res == 'fail') {
            return redirect()->back()->withErrors('Sorry!Email does not exist');
        } else {
            return redirect()->back()->with('emailFound', 'Successfully,Please check your email');
        }
    }

    /**
     * User can redirect to forgot password page
     *
     * @return \resources\views\UserViews\forgot-pass.blade.php
     */
    public function viewForgotPasswordPage(){
        return view('UserViews.forgot-pass');
    }
    /**
     * User can view their profile
     * get user's profile by current user id
     *
     * @return \resources\views\UserViews\profile.blade.php with object user
     */
    public function viewProfile()
    {
        $user = User::find(Auth::user()->id);
        return view('UserViews.profile', compact('user'));
    }

    /**
     * receive object from changeInformation() function at Model User
     *
     * @param ChangeInfoRequest $request
     * @return \resources\views\UserViews\profile.blade.php with message 'changeInfoSuccess' or error message
     */
    public function changeInformation(ChangeInfoRequest $request)
    {
        $user = new User();
        $res = $user->changeInformation();
        if ($res == 'fail') {
            return redirect()->back()->withErrors('Something wrong');
        } else {
            alert()->success('Change Information Successful')->persistent('Close');
            return redirect()->back()->with('changeInfoSuccess', 'Change Information Successful');
        }
    }

    /**
     * Manager can change User's information
     * receive object from changeInformationByManager() function at Model User
     *
     * @param ChangeInfoRequest $request
     * @return \resources\views\UserViews\view_profile_by_manager.blade.php with message 'changeInfoSuccess' or error message
     */
    public function changeInformationByManager(ChangeInfoRequest $request)
    {
        $user = new User();
        $res = $user->changeInformationByManager();
        if ($res == 'fail') {
            return redirect()->back()->withErrors('Something wrong');
        } else {
            alert()->success('Change Information Successful')->persistent('Close');
            return redirect()->route('director.view')->with('changeInfoSuccess', 'Change Information Successful');
        }
    }

    /**
     * User can change their password
     * receive response from changePassword() function at Model user
     *
     * @param ChangePasswordRequest $request
     * @return \resources\views\UserViews\profile.blade.php with message 'changePassSuccess' or error message
     */
    public function changePassword(ChangePasswordRequest $request)
    {
        $user = new User();
        $res = $user->changePassword();
        if ($res) {
            return redirect()->back()->with('changePassSuccess', 'Change Password Successful');
        } else {
            return redirect()->back()->withErrors('Current password is wrong');
        }
    }

    /**
     * User can view list department they are managing
     *
     * @return \resources\views\UserViews\list-staff-in-department-datatable.blade.php
     */
    public function viewListStaffDepartmentDatatable()
    {
        return view('UserViews.list-staff-in-department-datatable');
    }

    /**
     * get request from datatable and passing that to function at model
     * receive json for list staff in department and passing to view
     *
     * @param Request $request
     */
    public function getListStaff(Request $request)
    {
        $user = new User();
        $start = $request->input('start') ?: 0;
        $length = $request->input('length') ?: 10;
        $search = $request->input('search.value') ?: "";
        $oderColumn = $request->input('order.0.column') ?: 0;
        $oderSortType = $request->input('order.0.dir') ?: 'asc';
        $draw = $request->draw ?: 0;
        try {
            $output = $user->getListStaffInDepartment($start, $length, $search, $oderColumn, $oderSortType, $draw);
        } catch (\Exception $ex) {
            $output = array(
                "draw" => intval(0),
                "recordsTotal" => intval(0),
                "recordsFiltered" => intval(0),
                "data" => array($start . "," . $length . "," . $search . "," . $oderColumn . "," . $oderSortType . "," . $draw),
            );
        }
        echo json_encode($output);
    }


    public function exportToExcel(Request $request)
    {
//        dd($request->input('search.value') ?: "");
        return Excel::download(new UsersExportExcel(), 'users.xlsx');
    }

    /**
     * User can change their avatar
     * receive object from changeAvatar() function at Model User
     *
     * @param Request $request
     * @return \resources\views\UserViews\profile.blade.php with message 'updateAvatarSuccess' or error message
     */
    public function changeAvatar(Request $request)
    {
        $user = new User();
        $res = $user->changeAvatar($request);
        if ($res) {
            return redirect()->back()->with('updateAvatarSuccess', 'message');
        } else {
            return redirect()->back()->with('errorFile', 'Only accept PNG and JPG files');
        }
    }

    /**
     * View list staff in the department with department_id
     *
     * @return \resources\views\UserViews\list-employees-in-department.blade.php
     */
    public function viewListStaff()
    {
        $department_id = \request('id');
        return view('UserViews.list-employees-in-department', compact('department_id'));
    }

    /**
     * get request from datatable and passing that to function at model
     * receive json for list staff and passing to view
     *
     * @param Request $request
     */
    public function getJsonListStaff(Request $request)
    {
        $department_id = \request('id');
        $user = new User();
        $start = $request->input('start') ?: 0;
        $length = $request->input('length') ?: 10;
        $search = $request->input('search.value') ?: "";
        $oderColunm = $request->input('order.0.column') ?: 0;
        $oderSortType = $request->input('order.0.dir') ?: 'asc';
        $draw = $request->draw ?: 0;
        try {
            if ($department_id == ''){
                $user = new User();
                $output = $user->viewListNewEmployees($start, $length, $search, $oderColunm, $oderSortType, $draw);
            }else{
                $output = $user->getJsonListStaff($start, $length, $search, $oderColunm, $oderSortType, $draw, $department_id);
            }
        } catch (\Exception $ex) {
            $output = array(
                "draw" => intval(0),
                "recordsTotal" => intval(0),
                "recordsFiltered" => intval(0),
                "data" => array($start . "," . $length . "," . $search . "," . $oderColunm . "," . $oderSortType . "," . $draw),
            );
        }
        echo json_encode($output);
    }

    /**
     *Manager can view list department they managing.
     * @return \resources\views\UserViews\managing-department-page.blade.php with object departments,department_manager
     */
    public function viewListManagingDepartment()
    {
        $departments = DepartmentName::paginate(10);
        $department_manager = ManagerDepartment::where('user_id', Auth::user()->id)->get();
        return view('UserViews.managing-department-page', compact('departments', 'department_manager'));
    }

    public function searchListDepartmentManaging(){
        $search = \request('value');

        $departments = DepartmentName::where('name', 'like', "%$search%")->paginate(10);

        $department_manager = ManagerDepartment::whereHas('departmentN',function ($query) use ($search){
                $query->where('name', 'like', "%$search%");
            })
            ->where('user_id', Auth::user()->id)
            ->get();

        return view('UserViews.managing-department-page', compact('departments', 'department_manager'));
    }

    /**
     * Manager can leave from department managing
     */
    public function leaveManagerDepartment(){
        $managerDepartments = ManagerDepartment::where('user_id',Auth::user()->id)->get();
        foreach ($managerDepartments as $item){
            if($item->department_id==\request('id')){
                $item->delete();
            }
        }
    }

}
