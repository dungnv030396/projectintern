<?php

namespace App\Http\Controllers;

use App\ManagerDepartment;
use App\Role;
use Illuminate\Routing\Controller as BaseController;
use App\User;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    public function test()
    {
        $mDepartment = ManagerDepartment::where('user_id',Auth::user()->id)->get();
        $dId = array();
        foreach ($mDepartment as $item){
            array_push($dId, $item->department_id);
        }

        dd($dId);
    }
}
