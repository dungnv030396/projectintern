<?php

namespace App;

use App\Mail\ForgotPassword;
use App\Mail\WelcomeNewUser;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function userRoles()
    {
        return $this->hasMany(UserRole::class, 'user_id', 'id');
    }

    public function roles()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    public function departments()
    {
        return $this->hasOne(DepartmentName::class, 'id', 'department_id');
    }

    public function managerDepartments()
    {
        return $this->hasMany(ManagerDepartment::class, 'user_id', 'id');
    }

    public function managerDepartment()
    {
        return $this->belongsToMany(ManagerDepartment::class);
    }

    protected $fillable = [
        'name', 'email', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Login to system with username and password return user's role_id
     *
     * @return int role_id
     */
    public function login()
    {
        if (Auth::attempt(['username' => request('username'), 'password' => request('password')])) {
            return Auth::user()->role_id;
        } else {
            return 0;
        }
    }

    /**
     * logout and clear user's session
     */
    public function doLogout()
    {
        Auth::logout();
    }

    /**
     * get list users by Director without Director return list User $employees
     *
     * @return object $employees
     */
    public function getEmployeeByDirector()
    {
        $employees = User::where('role_id', '<>', 3)
            ->where('status', 1)
            ->paginate(10);
        return $employees;
    }

    /**
     * save data new account,create a token to user change password first time
     * and send a mail to user's email return $user
     * @return object $user or string 'email','user'
     */
    public function createNewAccount()
    {
        $emailCheck = User::where('email', request('email'))->first();
        $userCheck = User::where('username', request('username'))->first();
        if ($emailCheck) {
            return 'email';
        } else if ($userCheck) {
            return 'user';
        } else {
            $user = new User();
            $user->name = trim(request('name'));
            $user->email = trim(request('email'));
            $user->avatar = 'default.png';
            $user->birthday = Carbon::createFromFormat('d-m-Y', \request('birthday'));
            $user->address = trim(request('address'));
            $user->phone = trim(request('phone'));
            $user->username = trim(request('username'));
            $user->password = bcrypt(trim(request('password')));
            $user->save();

            $password_token = new PasswordToken();
            $password_token->user_id = $user->id;
            $randomString = $this->createNewToken();
            $password_token->token = bcrypt($randomString . '' . 'rikkei');
            $password_token->save();
            $data = [
                'user' => $user,
                'link' => route('check.url', $randomString),
            ];
            Mail::to($user)->send(new WelcomeNewUser($data));
            return $user;
        }
    }

    /**
     * create a token with 20 random character
     *
     * @return string
     */
    public function createNewToken()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 20; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * User forgot password with email,if true send a mail to user's email to reset password
     *
     * @return string 'fail' or object user
     */
    public function forgotPassword()
    {
        $user = User::where('email', \request('email'))->first();
        if (count($user) == 0) {
            return 'fail';
        } else {
            $randomString = $this->createNewToken();
            $PasswordToken = PasswordToken::where('user_id', $user->id)->first();
            if (count($PasswordToken) == 0) {
                $pToken = new PasswordToken();
                $pToken->user_id = $user->id;
                $pToken->token = bcrypt($randomString . '' . 'rikkei');
                $pToken->save();
            } else {
                $PasswordToken->token = bcrypt($randomString . '' . 'rikkei');
                $PasswordToken->status = 1;
                $PasswordToken->save();
            }
            $data = [
                'user' => $user,
                'link' => route('check.url', $randomString),
            ];
            Mail::to($user)->send(new ForgotPassword($data));
            return $user;
        }
    }

    /**
     * User can change their information
     *
     * @return string 'fail' or object user
     */
    public function changeInformation()
    {
        $user = User::find(Auth::user()->id);
        if (count($user) == 0) {
            return 'fail';
        } else {
            $user->name = request('name');
            $user->address = request('address');
            $user->birthday = Carbon::createFromFormat('d-m-Y', \request('birthday'));
            $user->phone = request('phone');
            $user->save();
            return $user;
        }
    }

    /**
     * Director can change User's information with user's id
     *
     * @return string 'fail' or object user
     */
    public function changeInformationByManager()
    {
        $user = User::find(request('id'));
        if (count($user) == 0) {
            return 'fail';
        } else {
            $user->name = request('name');
            $user->address = request('address');
            $user->birthday = Carbon::createFromFormat('d-m-Y', \request('birthday'));
            $user->phone = request('phone');
            $user->department_id = request('department_id');
            $user->save();

            $department_id = \request('department');
            if (!$department_id==null){
                foreach ($department_id as $item) {
                    $managerDepartment = new ManagerDepartment();
                    $managerDepartment->user_id = \request('id');
                    $managerDepartment->department_id = $item;
                    $managerDepartment->save();
                }
            }
            return $user;
        }
    }

    /**
     *User change password with current password return true or false
     *
     * @return bool
     */
    public function changePassword()
    {
        if (Hash::check(\request('current_password'), Auth::user()->password)) {
            Auth::user()->fill([
                'password' => Hash::make(request('password'))
            ])->save();
            return true;
        } else {
            return false;
        }
    }

    /**
     * User can chane their avatar with image file.
     *
     * @param $request
     * @return bool
     */
    public function changeAvatar($request)
    {
        $avatar = $request->file('avatar');
        $fileExtension = $avatar->GetClientOriginalExtension();
        $filename = $avatar->getClientOriginalName();
        $allowedfileExtension = ['jpg', 'png', 'PNG', 'JPG'];
        if (in_array($fileExtension, $allowedfileExtension)) {
            $filenameFinal = time() . '.' . $filename;
            $user = User::find(Auth::user()->id);
            $user->avatar = $filenameFinal;
            $user->save();
            $avatar->storeAs('public/avatar', $filenameFinal);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Director can search name,username,id of user and order by Created_at
     *
     * @return object $listEmployee
     */
    public function searchUser()
    {
        $value = \request('search');
        $listEmployee = User::whereHas('departments',function ($query) use ($value) {
            return $query->where('username', 'like', "%$value%")
                ->orWhere('name', 'like', "%$value%")
                ->orWhere('email', 'like', "%$value%")
                ->orWhere('phone', 'like', "%$value%");
        })
            ->where(function ($query) use ($value) {
                return $query->where('role_id', '<>', 3)
                    ->where('status', 1);
            })
            ->orderByRaw('created_at DESC')
            ->paginate(10);

        return $listEmployee;
    }

    /**
     * Get list staff in current user's department and sort by id,username,name,created_at,updated_at
     * search staff's information with username,id
     *
     * @param $start
     * @param $length
     * @param $search
     * @param $oderColunm
     * @param $oderSortType
     * @param $draw
     * @return array
     */
    public function getListStaffInDepartment($start, $length, $search, $oderColunm, $oderSortType, $draw)
    {
        $columns = array(
            0 => 'id',
            1 => 'username',
            2 => 'name',
            7 => 'created_at',
            8 => 'updated_at'
        );
        $mDepartment = ManagerDepartment::where('user_id', Auth::user()->id)->get();
        $dId = [];
        foreach ($mDepartment as $item) {
            array_push($dId, $item->department_id);
        }

        if (Auth::user()->department_id == null) {
            $totalData = User::whereIn('department_id', $dId)
            ->orwhereNull('department_id')->where('status', 1)->count();
        } else {
            $totalData = User::whereIn('department_id', $dId)
            ->orwhere('department_id', Auth::user()->department_id)->where('status', 1)->count();
        }
        if ($length < 0) {
            $length = $totalData;
        }
        if (empty($search)) {
//            $users = User::whereIn('department_id', $dId);
            if (Auth::user()->department_id == null) {
                $users = User::whereIn('department_id', $dId)
                    ->orwhereNull('department_id')
                    ->where('status', 1)
                    ->offset($start)
                    ->limit($length)
                    ->orderBy($columns[$oderColunm], $oderSortType)
                    ->get();
                $totalFiltered = $totalData;
            } else {
                $users = User::whereIn('department_id', $dId)
                    ->orwhere('department_id', Auth::user()->department_id)
                    ->where('status', 1)
                    ->offset($start)
                    ->limit($length)
                    ->orderBy($columns[$oderColunm], $oderSortType)
                    ->get();
                $totalFiltered = $totalData;
            }

        } else {
            $users = User::whereHas('departments', function ($query) use ($search) {
                $query->where('username', 'like', "%$search%")
                    ->orwhere('id', 'like', "%$search%")
                    ->orwhere('phone', 'like', "%$search%")
                    ->orwhere('name', 'like', "%$search%")
                    ->orwhere('email', 'like', "%$search%");
            })
                ->where(function ($query) use ($search,$dId) {
                    $query->where('status', 1)
                        ->whereIn('department_id', $dId);
                    if (Auth::user()->department_id == null) {
                        $query->orwhereNull('department_id');
                    } else {
                        $query->orwhere('department_id', Auth::user()->department_id);
                    }
                })
                ->offset($start)
                ->limit($length)
                ->orderBy($columns[$oderColunm], $oderSortType)
                ->get();
            $totalFiltered = $users->count();
        }
        $data = array();
        if ($users) {
            foreach ($users as $user) {
                $nestedData = array();
                $nestedData['id'] = $user->id;
                $nestedData['username'] = $user->username;
                $nestedData['name'] = $user->name;
                $nestedData['email'] = $user->email;
                $nestedData['phone'] = $user->phone;
                $nestedData['role_id'] = $user->role_id;
                $nestedData['address'] = $user->address;
                $user->department_id == null ? $nestedData['departmentName'] = '' : $nestedData['departmentName'] = $user->departments->name;
                $nestedData['created_at'] = $user->created_at->format('H:i:s d/m/Y');
                $nestedData['updated_at'] = $user->updated_at->format('H:i:s d/m/Y');

                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw" => intval($draw),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data,
        );
        return $json_data;
    }

    /**
     * Director can view list new Employees,can sort with id,username,name,created_at,updated_at
     * and search employees's information with username,id
     *
     * @param $start
     * @param $length
     * @param $search
     * @param $oderColunm
     * @param $oderSortType
     * @param $draw
     * @return array
     */
    public function viewListNewEmployees($start, $length, $search, $oderColunm, $oderSortType, $draw)
    {
        $columns = array(
            0 => 'id',
            1 => 'username',
            2 => 'name',
            6 => 'created_at',
            7 => 'updated_at'
        );
        $totalData = User::whereNull('department_id')->where('status', 1)->count();
        if ($length < 0) {
            $length = $totalData;
        }
        if (empty($search)) {
            $users = User::whereNull('department_id')
                ->where('status', 1)
                ->offset($start)
                ->limit($length)
                ->orderBy($columns[$oderColunm], $oderSortType)
                ->get();
            $totalFiltered = $totalData;
        } else {
            $users = User::where(function ($query) use ($search) {
                $query->where('username', 'like', "%$search%")
                    ->orwhere('id', 'like', "%$search%")
                    ->orwhere('phone', 'like', "%$search%")
                    ->orwhere('email', 'like', "%$search%");
            })
                ->where(function ($query) use ($search) {
                    $query->where('status', 1)
                        ->whereNull('department_id');
                })
                ->offset($start)
                ->limit($length)
                ->orderBy($columns[$oderColunm], $oderSortType)
                ->get();
            $totalFiltered = $users->count();
        }
        $data = array();
        if ($users) {
            foreach ($users as $user) {
                $nestedData = array();
                $nestedData['id'] = $user->id;
                $nestedData['username'] = $user->username;
                $nestedData['name'] = $user->name;
                $nestedData['email'] = $user->email;
                $nestedData['phone'] = $user->phone;
                $nestedData['role_id'] = $user->role_id;
                $nestedData['address'] = $user->address;
                $user->department_id == null ? $nestedData['departmentName'] = '' : $nestedData['departmentName'] = $user->departments->name;
                $nestedData['created_at'] = $user->created_at->format('H:i:s d/m/Y');
                $nestedData['updated_at'] = $user->updated_at->format('H:i:s d/m/Y');
                $nestedData['viewProfile'] = '<center><button class="btn btn-success"> <span class="glyphicon glyphicon-edit"></span><a href="' . route('view.profile.by.manager', $user->id) . '" style="color:white">' . ' Update' . '</a></button></center>';
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw" => intval($draw),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data
        );
        return $json_data;
    }

    /**
     * manager get list staff in each department by department_id
     * sort by id,username,name,created_at,updated_at
     * search staff's information with username,id
     *
     * @param $start
     * @param $length
     * @param $search
     * @param $oderColunm
     * @param $oderSortType
     * @param $draw
     * @param $department_id
     * @return array
     */
    public function getJsonListStaff($start, $length, $search, $oderColunm, $oderSortType, $draw, $department_id)
    {

        $columns = array(
            0 => 'id',
            1 => 'username',
            2 => 'name',
            7 => 'created_at',
            8 => 'updated_at'
        );
        $totalData = User::where('department_id', $department_id)->where('status', 1)->count();
        if ($length < 0) {
            $length = $totalData;
        }
        if (empty($search)) {
            $users = User::where('department_id', $department_id)
                ->where('status', 1)
                ->offset($start)
                ->limit($length)
                ->orderBy($columns[$oderColunm], $oderSortType)
                ->get();
            $totalFiltered = $totalData;
        } else {
            $users = User::where(function ($query) use ($search) {
                $query->where('username', 'like', "%$search%")
                    ->orwhere('id', 'like', "%$search%")
                    ->orwhere('phone', 'like', "%$search%")
                    ->orwhere('role_id', 'like', "%$search%");
            })
                ->where(function ($query) use ($search, $department_id) {
                    $query->where('status', 1)
                        ->where('department_id', $department_id);
                })
                ->offset($start)
                ->limit($length)
                ->orderBy($columns[$oderColunm], $oderSortType)
                ->get();
            $totalFiltered = $users->count();
        }
        $data = array();
        if ($users) {
            foreach ($users as $user) {
                $nestedData = array();
                $nestedData['id'] = $user->id;
                $nestedData['username'] = $user->username;
                $nestedData['name'] = $user->name;
                $nestedData['email'] = $user->email;
                $nestedData['phone'] = $user->phone;
                $nestedData['role_id'] = $user->role_id;
                $nestedData['address'] = $user->address;
                $nestedData['departmentName'] = $user->departments->name;
                $nestedData['created_at'] = $user->created_at->format('H:i:s d/m/Y');
                $nestedData['updated_at'] = $user->updated_at->format('H:i:s d/m/Y');
//                $nestedData['delete'] = '<center><button class="btn btn-danger"><a href="' . route('delete.user.department', $user->id) . '" style="color:white">' . 'Delete' . '</a></button></center>';
                $data[] = $nestedData;
            }
        }
        $json_data = array(
            "draw" => intval($draw),
            // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData),
            // total number of records
            "recordsFiltered" => intval($totalFiltered),
            // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data,
        );
        return $json_data;
    }
}
