<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManagerDepartment extends Model
{
    protected $fillable = ['user_id','department_id'];
//    public function departmentNames(){
//        return $this->hasMany(DepartmentName::class,'id','department_id');
//    }
    public function departmentN(){
        return $this->hasOne(DepartmentName::class,'id','department_id');
    }

    public function departmentName(){
        return $this->belongsTo(DepartmentName::class);
    }

    public function users(){
        return $this->hasOne(User::class,'id','user_id');
    }
}
