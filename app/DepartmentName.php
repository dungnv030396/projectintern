<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DepartmentName extends Model
{
    protected $fillable = ['name'];

    public function users(){
        return $this->belongsTo(User::class);
    }
    public function user(){
        return $this->hasMany(User::class,'department_id','id');
    }

    public function managerDepartments(){
        return $this->belongsToMany(ManagerDepartment::class);
    }

    public function managerDepartment(){
        return $this->hasMany(ManagerDepartment::class,'department_id','id');
    }

    /**
     * get list all Department
     *
     * @return object $listDepartment
     */
    public function getListDepartment(){
        $listDepartment = DB::table('department_names')
            ->select('manager_departments.user_id','manager_departments.department_id','department_names.id','department_names.created_at','department_names.updated_at','department_names.name','users.name as username')
            ->leftJoin('manager_departments','department_names.id','manager_departments.department_id')
            ->leftJoin('users','manager_departments.user_id','users.id')
            ->orderByRaw('department_names.id asc')
            ->paginate(5);

        return $listDepartment;
    }

    /**
     * get list Department has no manager
     *
     * @return object $listDepartment
     */
    public function getListDepartmentNoManager(){
        $listDepartment = DB::table('department_names')
            ->select('manager_departments.user_id','manager_departments.department_id','department_names.id','department_names.created_at','department_names.updated_at','department_names.name','users.name as username')
            ->leftJoin('manager_departments','department_names.id','manager_departments.department_id')
            ->leftJoin('users','manager_departments.user_id','users.id')
            ->where('user_id',null)
            ->orderByRaw('id asc')
            ->paginate(5);

        return $listDepartment;
    }

    /**
     * Director can search department name vs manager name
     * get value search request from director and process
     *
     * @return object $listDepartment
     */
    public function searchDepartmentName(){
        $search = \request('value');
        $listDepartment = DB::table('department_names')
            ->select('manager_departments.user_id','manager_departments.department_id','department_names.id','department_names.created_at','department_names.updated_at','department_names.name','users.name as username')
            ->leftJoin('manager_departments','department_names.id','manager_departments.department_id')
            ->leftJoin('users','manager_departments.user_id','users.id')
            ->where(function ($query) use ($search){
                 return $query->where('department_names.name', 'like', "%$search%")
                     ->orwhere('users.name','like',"%$search%");

            })
            ->orderByRaw('id asc')
            ->paginate(5);

        return $listDepartment;
    }
}
