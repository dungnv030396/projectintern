<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    public function users(){
        return $this->belongsTo(User::class);
    }


    public function roles(){
        return $this->belongsToMany(Role::class);
    }
}
