@extends('layouts.master')
@section('content')
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" type="text/css" href="css/css-register">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>

    <!------ Include the above in your HEAD tag ---------->
    <div class="container-fluid">
        <div class="row" style="margin: 50px 10px 50px 10px">
            <div class="panel-heading">
                <div class="panel-title text-center">
                    <h1 class="title">RikkeiSoft</h1>
                    <hr>
                </div>
            </div>
            <h1 class="title text-left">Register New Account Page</h1>
            <div class="main-login main-center">
                <form class="form-group" method="post" action="{{route('create.new.account')}}">

                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Your Name<span
                                    class="text-danger">*</span></label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"
                                                                   aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="name" placeholder="Enter your Name"
                                       required value="{{old('name')}}"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="cols-sm-2 control-label">Your Email<span class="text-danger">*</span></label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"
                                                                   aria-hidden="true"></i></span>
                                <input type="email" class="form-control" name="email" placeholder="Enter your Email"
                                       required value="{{old('email')}}"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="confirm" class="cols-sm-2 control-label">Your Address<span
                                    class="text-danger">*</span></label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-home"
                                                                   aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="address" placeholder="Enter your address"
                                       required value="{{old('address')}}"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="confirm" class="cols-sm-2 control-label">Your Phone<span
                                    class="text-danger">*</span></label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-phone"
                                                                   aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="phone" placeholder="Enter your phone"
                                       required value="{{old('phone')}}"/>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="confirm" class="cols-sm-2 control-label">Birth Day<span class="text-danger">*</span></label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"
                                                                   aria-hidden="true"></i></span>
                                <input type="text" class="datepicker form-control" data-date-format="mm-dd-yyyy"
                                       name="birthday" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="username" class="cols-sm-2 control-label">Username<span class="text-danger">*</span></label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"
                                                                   aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="username"
                                       placeholder="Enter your Username" required value="{{old('username')}}"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="cols-sm-2 control-label">Password<span class="text-danger">*</span></label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"
                                                                   aria-hidden="true"></i></span>
                                <input type="password" class="form-control" name="password"
                                       placeholder="Enter your Password"/>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="confirm" class="cols-sm-2 control-label">Confirm Password<span
                                    class="text-danger">*</span></label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"
                                                                   aria-hidden="true"></i></span>
                                <input type="password" class="form-control" name="password_confirmation"
                                       placeholder="Confirm your Password"/>
                            </div>
                        </div>
                    </div>


                    <div class="col-sm-12" style="text-align: center">
                        <div style="display: inline-block">
                            <button type="submit" class="btn btn-primary">Register</button>
                        </div>
                        <div style="display: inline-block">
                            <a href="{{route('list.new.employees') }}" type="button" class="btn btn-danger">Back</a>
                        </div>
                    </div>

                    <div class="form-group">
                        @include('layouts.errors')
                    </div>
                    <div class="form-group">
                        @if (\Illuminate\Support\Facades\Session::has('success'))
                            <div class="alert alert-success">
                                <ul>
                                    {{\Illuminate\Support\Facades\Session::get('success')}}
                                </ul>
                            </div>
                        @endif
                    </div>
                </form>
            </div>
        </div>
    </div>
    @if(\Illuminate\Support\Facades\Session::has('registerSuccess'))
        @include('sweet::alert')
    @endif
    <script>
        $.fn.datepicker.defaults.format = "dd-mm-yyyy";
        $('.datepicker').datepicker();
    </script>
@endsection