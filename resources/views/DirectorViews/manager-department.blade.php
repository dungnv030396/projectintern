@extends('layouts.master')
@section('content')
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <div class="container-fluid">
        <div class="row">
            <div class="panel-heading">
                <div class="panel-title text-center">
                    <h1 class="title">Manage All Departments</h1>
                    <hr/>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-1" style="margin: 25px 0 0 30px">
                    <a href="{{route('create.new.department')}}" type="button" class="btn btn-success"
                       style="margin: -40px 0px 20px -14px"><i class="glyphicon glyphicon-plus"></i>Add New
                        Department</a>
                </div>
                <div class="col-sm-1" style="float: right;margin:0 250px 20px 0">
                    <form action="{{route('search.department.name')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" name="value" class="form-control pull-right" style="width: 300px"
                                   placeholder="Search Something">

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Department List</h3>
                    </div>
                    <div class="form-group">
                        <table class="table table-bordered" id="table-department">
                            <tbody>
                            <tr>
                                <th style="width: 10px">ID</th>
                                <th>Department Name</th>
                                <th>Manager</th>
                                <th style="width: 200px">Created time</th>
                                <th style="width: 200px">Updated time</th>
                                <th style="width:155px">Action</th>
                            </tr>
                            @foreach($listDepartment as $item)
                                <tr>
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>
                                        {{--@foreach($item->user_id as $res)--}}
                                        @if($item->user_id!=null)
                                            <a href="{{route('view.profile.by.manager',$item->user_id)}}">{{ $item->username}}</a>
                                        @else
                                            <a href="">{{ $item->user_id}}</a>
                                        @endif
                                        {{--@endforeach--}}
                                    </td>
                                    <td>{{$item->created_at}}</td>
                                    <td>{{$item->updated_at}}</td>
                                    <td>
                                            <div style="float: left">
                                            <span><a href="{{route('view.detail.department',$item->id)}}"
                                                     class="btn btn-sm btn-primary"><span
                                                            class="glyphicon glyphicon-wrench"></span> Edit</a></span>
                                            </div>
                                            <div style="float: left">
                                                <a href="" class="btn btn-sm btn-danger" id="{{$item->id}}"
                                                   onclick="getConfirmation(this.id)" style="margin-left: 7px"><span
                                                            class="glyphicon glyphicon-floppy-remove"></span> Delete</a>
                                            </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-sm-1" style="float: right">
                        <div>
                            <a href="{{route('view.list.department')}}" type="button" class="btn btn-success">All
                                Manager</a>
                        </div>
                    </div>
                    <div class="col-sm-1" style="float: right">
                        <div>
                            <a href="{{route('view.list.department.no.manager')}}" type="button"
                               class="btn btn-success">Nope Manager</a>
                        </div>
                    </div>
                    <div class="col-sm-12" style="text-align: center">
                        <div style="display: inline-block">
                            {{$listDepartment->links()}}
                        </div>
                    </div>
                </div>
            </div>
            @if(\Illuminate\Support\Facades\Session::has('errorName'))
                @include('sweet::alert')
            @endif
            @if(\Illuminate\Support\Facades\Session::has('errorID'))
                @include('sweet::alert')
            @endif
            @if(\Illuminate\Support\Facades\Session::has('createSuccess'))
                @include('sweet::alert')
            @endif
        </div>
    </div>

    <script>
        function getConfirmation(id) {
            event.preventDefault(); // prevent form submit
            swal({
                title: "Are you sure?",
                text: "Do you want delete department ID: " + id + "!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, I want to delete!",
                cancelButtonText: "No, Cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "{{ route('delete.department') }}",
                        type: 'DELETE',
                        method: "POST",
                        dataType: "json",
                        data: {
                            "_token": "<?= csrf_token() ?>",
                            id: id
                        },
                        success: function () {

                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal("Success", "Deleted department ID: " + id);
                            window.location.reload(true);
                        }
                    })
                } else {
                    swal.close();
                }
            });
        }
    </script>
@endsection