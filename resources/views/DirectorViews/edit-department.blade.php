@extends('layouts.master')
@section('content')
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('select2/css/select2.min.css')}}">
    <!------ Include the above in your HEAD tag ---------->
    <div class="container-fluid">
        <div class="row" style="margin: 20px 20px 20px 0px">
            <div class="panel-heading">
                <div class="panel-title text-center">
                    <h1 class="title">RikkeiSoft</h1>
                    <hr>
                </div>
            </div>
            <h1 class="title text-left">Edit Department:</h1>

            <div class="main-login">
                <form class="form-group" method="post" action="{{route('edit.department')}}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Department Name<span
                                    class="text-danger">*</span></label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-font"
                                                                   aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="name" placeholder="Enter Department Name"
                                       required value="{{old('dname').$departments->name}}"/>
                                <input type="text" hidden value="{{$departments->id}}" name="department_id">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="cols-sm-2 control-label">Manager <span
                                    class="text-danger">*</span></label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"
                                                                   aria-hidden="true"></i></span>
                                <select name="manager_id" class="js-example-basic-single form-control">
                                    @if(is_null($mDepartment))<option value=""></option> @endif
                                    @foreach($managers as $item)
                                        <option class="select2-selection__rendered select2-container--below"
                                                value="{{$item->id}}" @if(!is_null($mDepartment)){{$mDepartment->users->id == $item->id?'selected':''}}@endif>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12"  style="text-align: center">
                            <button type="submit" class="btn btn-primary btn-lg login-button" style="display: inline-block">
                                Change
                            </button>
                            <button class="btn btn-danger btn-lg login-button" style="display: inline-block" id="{{$departments->id}}" onclick="deleteManager(this.id)">
                                Delete Manager
                            </button>
                        </div>
                    </div>
                    <div class="form-group">
                        @include('layouts.errors')
                    </div>
                </form>
            </div>
        </div>
    </div>
    @if(\Illuminate\Support\Facades\Session::has('errorName'))
        @include('sweet::alert')
    @endif

    @if(\Illuminate\Support\Facades\Session::has('errorID'))
        @include('sweet::alert')
    @endif

    <script src="{{asset('select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-single').select2({
                placeholder: 'Enter manager name',

            });

        });

        function deleteManager(id) {
            event.preventDefault(); // prevent form submit
            swal({
                title: "Delete manager?",
                text: "Do you want delete manager's rights: " + id + "!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, I do!",
                cancelButtonText: "No, Cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "{{ route('delete.manager.rights') }}",
                        method: "POST",
                        dataType: "json",
                        data: {
                            "_token": "<?= csrf_token() ?>",
                            id: id
                        },
                        success: function (data) {
                            //do nothing
                        },
                        error :function (data) {
                            swal("Success", "Deleted manager's rights: " + id, "success");
                            window.location.href = '{{route("view.list.department")}}';
                        }
                    })
                } else {
                    swal.close();
                }
            });
        }
    </script>
@endsection