@extends('layouts.master')
@section('content')
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('select2/css/select2.min.css')}}">
    <!------ Include the above in your HEAD tag ---------->

    <div class="container-fluid">
        <div class="row" style="margin: 20px 20px 20px 0px">
            <div class="panel-heading">
                <div class="panel-title text-center">
                    <h1 class="title">RikkeiSoft</h1>
                    <hr>
                </div>
            </div>
            <h1 class="title text-left">Create New Department:</h1>

            <div class="main-login">
                <form class="form-group" method="post" action="{{route('create.department')}}">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name" class="cols-sm-2 control-label">Department Name<span
                                    class="text-danger">*</span></label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-font"
                                                                   aria-hidden="true"></i></span>
                                <input type="text" class="form-control" name="name" placeholder="Enter Department Name"
                                       required value="{{old('dname')}}"/>
                                <input type="text" hidden value="" name="department_id">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="email" class="cols-sm-2 control-label">Manager <span
                                    class="text-danger">*</span></label>
                        <div class="cols-sm-10">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"
                                                                   aria-hidden="true"></i></span>
                                <select name="manager_id" class="js-example-basic-single form-control">
                                    <option value=""></option>
                                    @foreach($managers as $item)
                                        <option class="select2-selection__rendered select2-container--below"
                                                value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12"  style="text-align: center">
                            <button type="submit" class="btn btn-primary" style="display: inline-block">
                                Create
                            </button>
                            <a href="{{route('view.list.department')}}" type="button" class="btn btn-danger" style="display: inline-block">
                                Back
                            </a>
                        </div>
                    </div>
                    <div class="form-group">
                        @include('layouts.errors')
                    </div>
                </form>
            </div>
        </div>
    </div>
    @if(\Illuminate\Support\Facades\Session::has('createSuccess'))
        @include('sweet::alert')
    @endif

    @if(\Illuminate\Support\Facades\Session::has('errorName'))
        @include('sweet::alert')
    @endif

    <script src="{{asset('select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.js-example-basic-single').select2({
                placeholder: 'Enter manager name',

            });

        });

    </script>
@endsection