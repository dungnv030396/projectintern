@extends('layouts.master')
@section('content')
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <section class="content-header">
        <h1>
            Welcome to Director Page
        </h1>
        <ol class="breadcrumb">
            <li><a href=""><i class="fa fa-dashboard"></i>
                    Home</a></li>
            <li><a href="">Director</a></li>
            <li class="active">Manager</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row">

            <div class="col-xs-12">

                <div class="box">
                    <form action="{{ route('search.user') }}" method="POST" style="margin: 20px">
                        {{ csrf_field() }}
                    <div class="box-header" style="margin-bottom: 20px">
                        <div class="box-tools" >
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="search" class="form-control pull-right" style="width: 300px"
                                       placeholder="Enter something">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                    <form action="{{route('reset.password')}}" method="POST">
                    {{ csrf_field() }}
                    <!-- /.box-header -->
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover table-triped" id="table-list-user" style="margin-top: 20px">
                                <tbody>
                                <tr>
                                    <th style="width: 10px"><input type="checkbox" id="select_all" class="checkbox"></th>
                                    <th>Number</th>
                                    <th>User Name</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th style="max-width: 30px">Address</th>
                                    <th>Role Name</th>
                                    <th class="text-center">Department</th>
                                    <th>Created time</th>
                                    <th>Updated time</th>
                                    <th class="text-center">Delete</th>
                                    <th class="text-center">Update</th>
                                </tr>
                                <?php $i = 9;?>
                                @foreach($listEmployee as $employee)
                                    <tr>
                                        <td><input type="checkbox" name="checkbox_id[]" value="{{$employee->id}}" class="checkbox"></td>
                                        <td>{{ ($listEmployee->currentPage()*10) - $i }}</td>
                                        <?php $i--;?>
                                        <td>{{$employee->username}}</td>
                                        <td>{{$employee->name}}</td>
                                        <td>{{$employee->email}}</td>
                                        <td>{{$employee->phone}}</td>
                                        <td>{{$employee->address}}</td>
                                        <td>
                                            @if($employee->role_id==2)<span class="label label-success">{{$employee->roles->role_name}}</span>
                                            @elseif($employee->role_id==1)<span class="label label-primary">{{$employee->roles->role_name}}</span>
                                            @elseif($employee->role_id==4)<span class="label label-warning">{{$employee->roles->role_name}}</span>@endif
                                        </td>
                                        <td style="text-align: center">@if(!is_null($employee->department_id)){{$employee->departments->name}}@endif</td>
                                        <td>{{$employee->created_at}}</td>
                                        <td>{{$employee->updated_at}}</td>
                                        <td>
                                            <div class="text-center">
                                                <a href="" class="btn btn-sm btn-danger" id="{{$employee->id}}"
                                                   onclick="getConfirmation(this.id)"><span
                                                            class="	glyphicon glyphicon-remove"></span> Delete</a>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="text-center">
                                                <a href="{{route('view.profile.by.manager',$employee->id)}}"
                                                   class="btn btn-sm btn-success"><span
                                                            class="glyphicon glyphicon-edit"></span> Update</a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="col-sm-12" style="text-align: center">
                                <div style="display: inline-block">
                                {{ $listEmployee->links() }}
                                </div>
                            </div>
                        </div>

                        <!-- /.box-body -->
                </div>
                <div class="text-left">
                    <input class="btn btn-sm btn-danger" type="submit" value="Reset Password">
                </div>
                <!-- /.box -->
                </form>
                @if(\Illuminate\Support\Facades\Session::has('noSelect'))
                    @include('sweet::alert')
                @endif
                @if(\Illuminate\Support\Facades\Session::has('resetSuccess'))
                    @include('sweet::alert')
                @endif
                @if(\Illuminate\Support\Facades\Session::has('changeInfoSuccess'))
                    @include('sweet::alert')
                @endif
            </div>

        </div>
    </section>
    <script>
        function getConfirmation(id) {
            event.preventDefault(); // prevent form submit
            swal({
                title: "Delete account?",
                text: "Do you want delete this account: " + id + "!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, I do!",
                cancelButtonText: "No, Cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "{{ route('delete.user') }}",
                        method: "POST",
                        dataType: "json",
                        data: {
                            "_token": "<?= csrf_token() ?>",
                            id: id
                        },
                        success: function (data) {
                            if (data.error.length > 0) {
                                swal('Cancelled', "Đã có lỗi xảy ra!", "error");
                            }
                            else {
                                swal("Thành Công", "Đã khóa tài khoản: " + email, "success");
                            }
                        },
                        error :function (data) {
                            swal("Success", "Deleted account ID: " + id, "success");
                            window.location.reload(true);
                        }
                    })
                } else {
                    swal.close();
                }
            });
        }
        var select_all = document.getElementById("select_all"); //select all checkbox
        var checkboxes = document.getElementsByClassName("checkbox");

        select_all.addEventListener("change", function(e){
            for (i = 0; i < checkboxes.length; i++) {
                checkboxes[i].checked = select_all.checked;
            }
        });
        for (var i = 0; i < checkboxes.length; i++) {
            checkboxes[i].addEventListener('change', function(e){ //".checkbox" change
                //uncheck "select all", if one of the listed checkbox item is unchecked
                if(this.checked == false){
                    select_all.checked = false;
                }
                //check "select all" if all checkbox items are checked
                if(document.querySelectorAll('.checkbox:checked').length == checkboxes.length){
                    select_all.checked = true;
                }
            });
        }

    </script>
@endsection