@extends('layouts.master')
@section('content')
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="{{asset('source/css/datatables.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('datatables/custom-datatable.css')}}">

    <section class="content-header">
        <h1>
            List of Department staff Page
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row" style="float: right;margin-right: 100px">
            <div class="col-xs-1">
                <a href="{{route('register')}}" type="button" class="btn btn-success" style="margin-top: -40px"><i class="glyphicon glyphicon-plus"></i> Add New Account</a>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding margin-table">
                        <table class="table table-hover table-list-user" id="table-list-user" >
                            <thead>
                            <tr >
                                <th>ID</th>
                                <th>User Name</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Created time</th>
                                <th>Updated time</th>
                                <th style="text-align: center">Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>

        </div>
    </section>
    <script src="{{asset('source/js/jquery-2.1.1.js')}}"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $('.table-list-user').DataTable({
                "lengthMenu": [[10,25, 100, -1], [10,25, 100, "All"]],
                "pageLength": 25,
                "processing": true,
                "serverSide": true,
                "responsive": true,
                "stateSave": true,
                "stateDuration": -1,
                "language": {
                    "paginate": {
                        "previous": " < ",
                        "next": " > "
                    }
                    },
                "autoFill": true,
                "order": [[0, 'asc']],
                "ajax": {
                    "url": "<?= route('get.list.new.employees')?>",
                    "dataType": "json",
                    "type": "POST",
                    "data": {"_token": "<?= csrf_token() ?>"}
                },
                "columns":
                    [
                        {
                            data: "id",
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<span class='text-success'><b>" + oData.id + "</b><span>");
                            }
                        }, {
                        data: "username"
                        },
                        {
                            data: "name"
                        },
                        {
                            data: "email",orderable:false
                        },
                        {
                            data: "phone",orderable:false
                        },
                        {
                            data: "address",orderable:false
                        },
                        {
                            data: "created_at"
                        },
                        {
                            data: "updated_at"
                        },
                        {
                          data: "viewProfile",orderable:false
                        },
                    ],
                columnDefs: [
                    {className: 'control'},
                    {orderable: false},
                    {responsivePriority: 1, targets: 0},
                    {responsivePriority: 2, targets: -1},
                    {responsivePriority: 3, targets: 3},
                    {responsivePriority: 4, targets: 6},
                ],
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ],

                });
        });
    </script>


@endsection
