<!DOCTYPE html>
<html style="height: auto; min-height: 100%;">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Project Intern</title>
    <!-- SweetAlert's library -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{asset('css/_all-skins.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/AdminLTE.min.css')}}">
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <script src="{{asset('js/jquery.min.js')}}"></script>

</head>
<body class="skin-blue sidebar-mini" style="height: auto; min-height: 100%;">
<div class="wrapper" style="height: auto; min-height: 100%;">

    <header class="main-header">
        <!-- Logo -->
        <a class="logo">
            <span class="logo-lg"><b>Home</b></span>
        </a>
        @include('layouts.nav')
    </header>
    @include('layouts.sidebar')

    <div class="content-wrapper" style="min-height: 960px;">
        @yield('content')
    </div>

</div>
@if(\Illuminate\Support\Facades\Session::has('noRights'))
    @include('sweet::alert')
@endif
@if(\Illuminate\Support\Facades\Session::has('404notfound'))
    @include('sweet::alert')
@endif
{{--<script src="{{asset('js/analytics.js')}}"></script>--}}
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/jquery.slimscroll.min.js')}}"></script>
{{--<script src="{{asset('source/js/jquery-2.1.1.js')}}"></script>--}}
<script src="{{asset('source/js/datatables.min.js')}}"></script>
<script src="{{asset('js/fastclick.js')}}"></script>
<script src="{{asset('js/adminlte.min.js')}}"></script>
{{--<script src="{{asset('js/demo.js')}}"></script>--}}


</body>
</html>
