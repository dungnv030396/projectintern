<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="height: auto;">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ URL::to('/') }}/storage/avatar/{{\Illuminate\Support\Facades\Auth::user()->avatar}}"
                     class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{\Illuminate\Support\Facades\Auth::user()->name}}</p>
            </div>
        </div>
        <ul class="sidebar-menu tree" data-widget="tree">

            <li class="treeview">
                <a href="">
                    <i class="glyphicon glyphicon-user"></i> <span>Manage User</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('director.view')}}"><i class="glyphicon glyphicon-list-alt"></i>List User</a></li>
                    <li><a href="{{route('list.new.employees')}}"><i class="glyphicon glyphicon-list-alt"></i>List New
                            Employees</a></li>
                </ul>
            </li>
            <li class="form-group">
                <a href="{{route('view.list.department')}}"><i class="glyphicon glyphicon-cog"></i>Manage All Department</a>
            </li>
            <li class="form-group">
                <a href="{{route('view.list.staff.department.datatable')}}">
                    <i class="glyphicon glyphicon-user"></i> <span>User In Your Department</span>
                </a>
            </li>
            <li class="form-group">
                <a href="{{route('view.managing.department.page')}}"><i class="glyphicon glyphicon-list-alt"></i>Managing Department</a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>