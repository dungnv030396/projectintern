<nav class="navbar navbar-static-top">
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li class="dropdown user user-menu">
                <a href="" class="dropdown-toggle" data-toggle="dropdown">
                    <img src="{{ URL::to('/') }}/storage/avatar/{{\Illuminate\Support\Facades\Auth::user()->avatar}}" class="user-image" alt="User Image">
                    <span class="hidden-xs">{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <img src="{{ URL::to('/') }}/storage/avatar/{{\Illuminate\Support\Facades\Auth::user()->avatar}}" class="img-circle" alt="User Image">

                        <p>
                            {{\Illuminate\Support\Facades\Auth::user()->name }} - Web Developer
                            <small>{{\Illuminate\Support\Facades\Auth::user()->birthday}}</small>
                        </p>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="{{route('profile')}}" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="{{route('logout')}}" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>