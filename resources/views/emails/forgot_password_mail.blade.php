<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>Document</title>
</head>
<body>
<h1>Welcome to Rikkeisoft, {{ $data['user']->name }}</h1>
<br>
<h3 style="color: orange">You have sent a request to reset the password successfully.</h3>
<br>
<h3>Your UserName: {{  $data['user']->username }}</h3>
<p>Click here to change password: <a href="{{$data['link']}}">Link</a></p>
</body>
</html>