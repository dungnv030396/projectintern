@extends('layouts.master')
@section('content')
    <!-- SweetAlert's library -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <h1>Profile Page</h1>
                    <div class="tab-content">
                        <div class="tab-pane active" id="settings">
                            <div class="row">
                                <div class="col-sm-offset-5">
                                    <form method="POST" action="{{route('change.avatar')}}"
                                          enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="text-center">
                                                <img src="{{ URL::to('/') }}/storage/avatar/{{\Illuminate\Support\Facades\Auth::user()->avatar}}"
                                                     class="avatar img-circle img-thumbnail" alt="avatar">
                                                <h6>Change other avatar here!!!</h6>
                                                <input type="file" class="text-center center-block well well-sm"
                                                       name="avatar" id="avatar" required>

                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <button type="submit" class="btn btn-danger">Update Avatar
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        @if (\Illuminate\Support\Facades\Session::has('errorFile'))
                                            <div class="alert alert-danger">
                                                <ul>
                                                    {{\Illuminate\Support\Facades\Session::get('errorFile')}}
                                                </ul>
                                            </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <br>
                            <form class="form-horizontal" method="post" action="{{route('change.information')}}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Name</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name" placeholder="Name"
                                               value="{{$user->name}}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">UserName</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="username" placeholder="UserName"
                                               value="{{$user->username}}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" name="email" placeholder="Email"
                                               value="{{$user->email}}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Address</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="address" placeholder="Address"
                                               value="{{$user->address}}" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">BirthDay</label>

                                    <div class="col-sm-10">
                                        <div class="input-group date" data-provide="datepicker">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                            <input type="text" class="datepicker form-control"
                                                   data-date-format="mm-dd-yyyy"
                                                   value="{{date('d-m-Y', strtotime($user->birthday)) }}"
                                                   name="birthday" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Phone</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="phone" placeholder="Phone"
                                               value="{{$user->phone}}" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger">Submit</button>
                                    </div>
                                </div>
                            </form>
                            @if (\Illuminate\Support\Facades\Session::has('changeInfoSuccess'))
                                @include('sweet::alert')
                            @endif
                            @if(\Illuminate\Support\Facades\Session::has('noRight'))
                                @include('sweet::alert')
                            @endif
                            <hr>

                            <form class="form-horizontal" method="post" action="{{route('change.password')}}">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Current Password</label>

                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" name="current_password"
                                               placeholder="Current Password" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">New password</label>

                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" name="password"
                                               placeholder="New password" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Confirmation New
                                        password</label>

                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" name="password_confirmation"
                                               placeholder="Confirmation password" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    @if (\Illuminate\Support\Facades\Session::has('changePassSuccess'))
                                        <div class="alert alert-success">
                                            <ul>
                                                {{\Illuminate\Support\Facades\Session::get('changePassSuccess')}}
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    @include('layouts.errors')
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <script>
            $.fn.datepicker.defaults.format = "dd-mm-yyyy";
            $('.datepicker').datepicker();
        </script>
    </section>

@endsection