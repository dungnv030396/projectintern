@extends('layouts.master')
@section('content')
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script src="{{asset('js/jquery.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <div class="nav-tabs-custom">
                    <h1>User: {{$user->name}} and ID: {{$user->id}}</h1>
                    <div class="tab-content">
                        <div class="tab-pane active" id="settings">
                            <form class="form-horizontal" method="post"
                                  action="{{route('change.information.by.manager')}}">
                                {{ csrf_field() }}
                                <input type="text" hidden value="{{$user->id}}" name="id">
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Name</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name" placeholder="Name"
                                               value="{{$user->name}}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">UserName</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="username" placeholder="UserName"
                                               value="{{$user->username}}" readonly>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">Department</label>

                                    <div class="col-sm-10">
                                        <select class="form-control" name="department_id">
                                            @if($user->department_id==null)
                                                <option value=""
                                                >Nope
                                                </option>
                                            @endif
                                            @foreach($departments as $department)
                                                <option value="{{$department->id}}"
                                                        @if($user->department_id==$department->id)
                                                        selected
                                                        @endif
                                                >{{$department->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" name="email" placeholder="Email"
                                               value="{{$user->email}}" readonly>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Address</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="address" placeholder="Address"
                                               value="{{$user->address}}" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">BirthDay</label>

                                    <div class="col-sm-10">
                                        <div class="input-group date" data-provide="datepicker">
                                            <div class="input-group-addon">
                                                <span class="glyphicon glyphicon-th"></span>
                                            </div>
                                            <input type="text" class="datepicker form-control"
                                                   data-date-format="mm-dd-yyyy"
                                                   value="{{date('d-m-Y', strtotime($user->birthday)) }}"
                                                   name="birthday" readonly>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputName" class="col-sm-2 control-label">Phone</label>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="phone" placeholder="Phone"
                                               value="{{$user->phone}}" required>
                                    </div>
                                </div>
                                <hr>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">Department's
                                        managing</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="department_id" multiple>
                                            @foreach($dName as $name)
                                                <option value="volvo">{{$name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-sm-2 control-label">All Department</label>

                                    <div class="col-sm-10">
                                        <select class="form-control" name="department[]" multiple>
                                            @foreach($departments as $department)
                                                @if(in_array($department->name,$dName))
                                                    <option value="{{$department->id}}"
                                                            hidden>{{$department->name}}</option>
                                                    {{--@elseif($department->id==1)--}}
                                                    {{--<option value="{{$department->id}}"--}}
                                                    {{--hidden>{{$department->name}}</option>--}}
                                                @else
                                                    <option value="{{$department->id}}">{{$department->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <input type="text" hidden value="{{$user->id}}" name="id">

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button type="submit" class="btn btn-danger">Submit</button>
                                    </div>
                                </div>
                            </form>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        @if (\Illuminate\Support\Facades\Session::has('changeInfoSuccess'))
                                            @include('sweet::alert')
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    @include('layouts.errors')
                                </div>
                            </div>
                        </div>
                    @if(\Illuminate\Support\Facades\Session::has('assignmentSuccess'))
                        @include('sweet::alert')
                    @endif
                    @if(\Illuminate\Support\Facades\Session::has('selectNull'))
                        @include('sweet::alert')
                    @endif
                    @if(\Illuminate\Support\Facades\Session::has('noDepartment'))
                        @include('sweet::alert')
                    @endif

                    <!-- /.tab-pane -->
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.nav-tabs-custom -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
        <script>
            $.fn.datepicker.defaults.format = "dd-mm-yyyy";
            $('.datepicker').datepicker();
        </script>
    </section>
@endsection