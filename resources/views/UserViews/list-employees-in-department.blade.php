@extends('layouts.master')
@section('content')
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

    <link href="{{asset('source/css/datatables.min.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('datatables/custom-datatable.css')}}">



    <section class="content-header">
        <h1>
            List of User In Your Managing Department
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <div class="row">

            <div class="col-xs-12 ">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding margin-table">
                        <table class="table table-hover table-list-user" id="table-list-user">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>User Name</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                                <th>Department</th>
                                <th>Created time</th>
                                <th>Updated time</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <div class="row">
                <div class="col-sm-12"  style="text-align: right">
                    <a href="{{route('view.managing.department.page')}}" type="button" class="btn btn-danger" style="display: inline-block; margin-right: 50px">
                        Back
                    </a>
                </div>
            </div>
        </div>
    </section>
    @if(\Illuminate\Support\Facades\Session::has('error'))
        @include('sweet::alert')
    @endif
    <script src="{{asset('source/js/jquery-2.1.1.js')}}"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            $('.table-list-user').DataTable({
                "lengthMenu": [[10,25, 100, -1], [10,25, 100, "All"]],
                "pageLength": 25,
                "processing": true,
                "serverSide": true,
                "responsive": true,
                "stateSave": true,
                "stateDuration": -1,
                "order": [[0, 'asc']],
                "language": {
                    "paginate": {
                        "previous": " < ",
                        "next": " > "
                    }
                },
                "autoFill": true,
                "ajax": {
                    "url": "<?= route('get.json.list.staff.in.department', $department_id)?>",
                    "dataType": "json",
                    "type": "POST",
                    "data": {"_token": "<?= csrf_token() ?>"}
                },
                "columns":
                    [
                        {
                            data: "id",
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<span class='text-success'><b>" + oData.id + "</b></span>");
                            }
                        }, {
                        data: "username"
                    },
                        {
                            data: "name"
                        },
                        {
                            data: "email", orderable: false
                        },
                        {
                            data: "phone", orderable: false
                        },
                        {
                            data: "address", orderable: false
                        },
                        {
                            data: "departmentName", orderable: false
                        },
                        {
                            data: "created_at"
                        },
                        {
                            data: "updated_at"
                        },
                        {
                            data: "id",
                            "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                                $(nTd).html("<center><a type='button' href='' id='" + oData.id + "' class='btn btn-danger' onclick='getConfirmation(this.id)'>Delete</a></center>");
                            },
                            orderable: false
                        }
                    ],
                columnDefs: [
                    {className: 'control'},
                    {orderable: false},
                    {responsivePriority: 1, targets: 0},
                    {responsivePriority: 2, targets: -1},
                    {responsivePriority: 3, targets: 3},
                    {responsivePriority: 4, targets: 6},
                ],
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'copy'},
                    {extend: 'excelHtml5', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');
                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]
            });
        });

        function getConfirmation(id) {
            event.preventDefault(); // prevent form submit
            swal({
                title: "Delete this account from the department?",
                text: "Do you want delete this account: " + id + "!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, I do!",
                cancelButtonText: "No, Cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "{{ route('delete.user.from.department') }}",
                        method: "POST",
                        dataType: "json",
                        data: {
                            "_token": "<?= csrf_token() ?>",
                            id: id
                        },
                        success: function (data) {
                            if (data.error.length > 0) {
                                swal('Cancelled', "Đã có lỗi xảy ra!", "error");
                            }
                            else {
                                swal("Thành Công", "Đã khóa tài khoản: " + email, "success");
                            }
                        },
                        error: function (data) {
                            swal("Success", "Deleted account ID: " + id, "success");
                            $('.table-list-user').DataTable().ajax.reload();
                        }
                    })
                } else {
                    swal.close();
                }
            });
        }
    </script>


@endsection
