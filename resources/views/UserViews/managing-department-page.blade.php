@extends('layouts.master')
@section('content')
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->

    <div class="container-fluid">
        <div class="row">
            <div class="panel-heading">
                <div class="panel-title text-center">
                    <h1 class="title">List Managing Departments</h1>
                    <hr/>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="box">
                    <div class="col-sm-1" style="float: right;margin: 30px 250px">
                        <form action="{{route('search.list.department.managing')}}" method="POST">
                            {{ csrf_field() }}
                        <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="value" class="form-control pull-right" style="width: 300px"
                                       placeholder="Search Something">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="glyphicon glyphicon-search"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="form-group">
                        <table class="table table-bordered" id="table-department" style="width: 97%;margin: 20px 20px 20px 24px">
                            <tbody>
                            <tr>
                                <th style="width: 10px">ID</th>
                                <th>Department Name</th>
                                <th style="width: 200px">Created time</th>
                                <th style="width: 200px">Updated time</th>
                                @if(\Illuminate\Support\Facades\Auth::user()->role_id != 3)<td style="width: 30px">Action</td>@endif
                                <th style="width: 30px">Action</th>
                            </tr>
                            @if(\Illuminate\Support\Facades\Auth::user()->role_id == 3)
                                @foreach($departments as $department)
                                    <tr>
                                        <td>{{$department->id}}</td>
                                        <td>{{$department->name}}</td>
                                        <td>{{$department->created_at}}</td>
                                        <td>{{$department->updated_at}}</td>
                                        <td>
                                            <div class="text-center">
                                                <a href="{{route('view.list.staff.in.department', $department->id)}}" class="btn btn-sm btn-success" id="{{$department->id}}"><span
                                                            class="glyphicon glyphicon-eye-open"></span> View List User</a>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                @foreach($departments as $department)
                                    @foreach($department_manager as $item)
                                        @if($item->department_id == $department->id)
                                            <tr>
                                                <td>{{$department->id}}</td>
                                                <td>{{$department->name}}</td>
                                                <td>{{$department->created_at}}</td>
                                                <td>{{$department->updated_at}}</td>
                                                <td>
                                                    <div class="text-center">
                                                        <a href="" class="btn btn-sm btn-danger" id="{{$department->id}}" onclick="leaveManager(this.id)"><span
                                                                    class="glyphicon glyphicon-log-out"></span> Leave</a>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class="text-center">
                                                        <a href="{{route('view.list.staff.in.department', $department->id)}}" class="btn btn-sm btn-success" id="{{$department->id}}"><span
                                                                    class="glyphicon glyphicon-eye-open"></span> View List User</a>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    {{ $departments->links() }}
                </div>
            </div>
        </div>
        </div>

    </div>
    <script src="{{asset('source/js/jquery-2.1.1.js')}}"></script>
    <script>
        function leaveManager(id) {
            event.preventDefault(); // prevent form submit
            swal({
                title: "Are you sure?",
                text: "Do you want leave manager department ID: " + id + "!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, I want to leave!",
                cancelButtonText: "No, Cancel!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url: "{{ route('leave.manager.department') }}",
                        type: 'DELETE',
                        method: "POST",
                        dataType: "json",
                        data: {
                            "_token": "<?= csrf_token() ?>",
                            id: id
                        },
                        success: function () {
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            swal("Success", "Leaved department ID: " + id);
                            window.location.reload(true);
                        }
                    })
                } else {
                    swal.close();
                }
            });
        }
    </script>
@endsection