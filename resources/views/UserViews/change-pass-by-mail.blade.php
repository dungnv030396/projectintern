<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="{{asset('css.css')}}">
<!------ Include the above in your HEAD tag ---------->

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">

<div class="content py-5 bg-light  ">
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <span class="anchor" id="formChangePassword"></span>
                <!-- form card change password -->
                <div class="card card-outline-secondary">
                    <div class="card-header">
                        <h3 class="mb-0">Change Password</h3>
                    </div>
                    <div class="card-body">
                        <form class="form" role="form" autocomplete="off" method="POST" action="{{route('change.password.first')}}">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="inputPasswordOld">New Password</label>
                                <input type="password" class="form-control" name="password" required>
                            </div>
                            <div class="form-group">
                                <label for="inputPasswordNew">Confirm Password</label>
                                <input type="password" class="form-control" name="password_confirmation" required>
                                <span class="form-text small text-muted">
                                            The password must be 8-20 characters, and must <em>not</em> contain spaces.
                                        </span>
                            </div>
                            <input type="text" hidden value="{{$user_id}}" name="user_id">
                            <div class="form-group">
                                <button type="submit" class="btn btn-success btn-lg float-right">Save</button>
                            </div>
                            <button class="btn btn-primary btn-lg float-left"><a href="{{route('home')}}" style="color: white">HomePage</a></button>
                        </form>


                    </div>
                    <div class="form-group">
                        @include('layouts.errors')
                    </div>
                </div>
                <!-- /form card change password -->

            </div>
        </div>
    </div>
</div>
